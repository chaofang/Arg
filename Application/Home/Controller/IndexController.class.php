<?php

namespace Home\Controller;

use Think\Controller;

class IndexController extends BaseController{

    public function _initialize()
    {
        //导航类
        $nav=M('category')->where('pid=0')->select();
        $this->assign('nav',$nav);
//        dd($nav);

        //轮播图
        $lunbo=M('lunbo')->where('status=1')->select();
        $this->assign('lunbo',$lunbo);

        //友情链接
        $links=M('links')->select();
        $this->assign('links',$links);

        //页面底部配置
        $footer=M("setting")->order('sort asc')->select();
        $this->assign('footer',$footer);

    }

    //分发路由
    public function route()
    {
        $action=I('get.act');
        //导航类
        $nav=M('category')->where('pid=0')->field('action')->select();
        $navs = array_column($nav, 'action');

        if(in_array($action,$navs)){
            $this->redirect('index/'.$action);
        }else{
            $this->redirect("index");
        }

    }
    
    //首页
    public function index(){
        $action=ACTION_NAME;
        $this->assign("action",$action);

        //首页内容
        $cate=M('category')->alias("b")->where('b.pid=1')->select();
         foreach($cate as $k=>$v){
            $cate[$k]['children']=M('post')->where('cate_id=%d',$v['id'])->select();
         }

         $this->assign("cate",$cate);

        $this->display();
    }


//    内容页
    public function content()
    {
        $action=ACTION_NAME;
        $this->assign("action",$action);
        //新闻页不同
        $id=I('get.id');
        $cates=M('category')->alias("b")->where('b.pid=%d',$id)->select();
        foreach($cates as $k=>$v){
            $cates[$k]['children']=M('post')->where('cate_id=%d',$v['id'])->select();
        }


    }


    //关于仁义邦
    public function about()
    {
        $action=ACTION_NAME;
        $this->assign("action",$action);
        $this->display();

    }

    //业务范畴
    public function business()
    {
        $action=ACTION_NAME;
        $this->assign("action",$action);

        $this->display();
    }

    //联系我们
    public function contact()
    {
        $action=ACTION_NAME;
        $this->assign("action",$action);

        $this->display();
    }

    //新闻咨讯
    public function news()
    {
        $action=ACTION_NAME;
        $this->assign("action",$action);

        $this->display();
    }

    //新闻咨讯
    public function service()
    {
        $action=ACTION_NAME;
        $this->assign("action",$action);

        $this->display();
    }
    
    
    
}
