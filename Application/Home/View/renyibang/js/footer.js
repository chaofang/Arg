function IFrameReSize(iframename) {
　　var pTar = document.getElementById(iframename);
　　if (pTar) { 
        // 高度自适应
    　　if (pTar.contentDocument && pTar.contentDocument.body.offsetHeight && pTar.contentDocument.body.offsetWidth) {
            //ff
    　　    pTar.height = pTar.contentDocument.body.offsetHeight;
            pTar.width = pTar.contentDocument.body.offsetWidth;
    　　} else if (pTar.Document && pTar.Document.body.scrollHeight && pTar.Document.body.scrollWidth) {
            //ie
    　　    pTar.height = pTar.Document.body.scrollHeight;
            pTar.width = pTar.Document.body.scrollWidth;
    　　}
    }
}

// 窗口大小改变重新调整iframe宽和高
window.onresize = function(){
    IFrameReSize('myiframe');
    location.reload();
}
 