<?php
namespace Admin\Controller;
use Admin\Controller;
/**
 * 单页管理
 */
class LunboController extends BaseController
{
    /**
     * 轮播图及广告管理
     * @return [type] [description]
     */
    public function index()
    {

        $model = M('lunbo');
        $where="1=1";
        $count  = $model->where($where)->count();// 查询满足要求的总记录数
        $lunbo = new \Extend\page($count,15);// 实例化分页类 传入总记录数和每页显示的记录数(25)

        $show = $lunbo->show();// 分页显示输出
        $lunbos = $model->limit($lunbo->firstRow.','.$lunbo->listRows)->where($where)->order('id DESC')->select();
        $this->assign('model', $lunbos);
        $this->assign('lunbo',$show);
        $this->display();     
    }

    /**
     * 添加轮播图
     */
    public function add()
    {
        //默认显示添加表单
        if (!IS_POST) {
            $this->display();
        }
        if (IS_POST){

            $data=I("post.");
           $res= $this->upload();
           $data['img']=$res['file']['filepath'];
           $data['img_name']=$res['file']['name'];

            //如果用户提交数据
            $model = D("lunbo");
            if (!$model->create()) {
                // 如果创建失败 表示验证没有通过 输出错误提示信息
                $this->error($model->getError());
                exit();
            } else {

                if ($model->add($data)) {
                    $this->success("添加成功", U('lunbo/index'));
                } else {
                    $this->error("添加失败");
                }
            }
        }
    }
    /**
     * 更新单页信息
     * @param  [type] $id [单页ID]
     * @return [type]     [description]
     */
    public function update($id)
    {
    		$id = intval($id);
        //默认显示添加表单
        if (!IS_POST) {
            $model = M('lunbo')->where("id=%d",$id)->find();
            $this->assign('lunbo',$model);
            $this->display();
        }
        if (IS_POST) {
            $model = D("lunbo");

                $data=I("post.");

                if($_FILES['file']['error']==0)//有图上传
                {
                    $res= $this->upload();
                    $data['img']=$res['file']['filepath'];
                    $data['img_name']=$res['file']['name'];
                }
                if ($model->save($data)) {
                    $this->success("更新成功", U('lunbo/index'));
                } else {
                    $this->error("更新失败");
                }        

        }
    }
    /**
     * 删除单页
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
    		$id = intval($id);
        $model = M('lunbo');
        $result = $model->where("id=%d",$id)->delete();
        if($result){
            $this->success("删除成功", U('lunbo/index'));
        }else{
            $this->error("删除失败");
        }
    }

    public function disuse($id)
    {
        $id = intval($id);

        $model = M('lunbo');
        //查询status字段值
        $result = $model->find($id);
        //更新字段
        $data['id']=$id;
        if($result['status'] == 1){
            $data['status']=0;
        }
        if($result['status'] == 0){
            $data['status']=1;
        }
        if($model->save($data)){
            $this->success("状态更新成功", U('lunbo/index'));
        }else{
            $this->error("状态更新失败");
        }

    }
}
