<?php
namespace Admin\Controller;
use Admin\Controller;
/**
 * 文章管理
 */
class postimgController extends BaseController
{
    /**
     * 文章列表
     * @return [type] [description]
     */
    public function index($key="")
    {
        if($key === ""){
            $model = D('postimg');
            $where="1=1";
        }else{
            $where['postimg.title'] = array('like',"%$key%");
            $where['member.username'] = array('like',"%$key%");
            $where['category.title'] = array('like',"%$key%");
            $where['_logic'] = 'or';
            $model = D('postimg')->where($where);
        }

        $count  = $model->where($where)->count();// 查询满足要求的总记录数
        $Page = new \Extend\Page($count,15);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        $postimg = $model->alias('a')
                ->join('category ON a.cate_id=category.id')
                ->field('a.*,category.title as category_title')
                ->limit($Page->firstRow.','.$Page->listRows)
                ->where($where)
                ->order('a.id DESC')
                ->select();
        $this->assign('model', $postimg);
        $this->assign('page',$show);
        $this->display();
    }
    /**
     * 添加文章
     */
    public function add()
    {
        //默认显示添加表单
        if (!IS_POST) {
            $this->assign("category",getSortedCategory(M('category')->select()));
            $this->display();
        }
        if (IS_POST) {
            //如果用户提交数据
            $model = D("postimg");
            $model->time = time();
            $model->user_id = 1;
            if (!$model->create()) {
                // 如果创建失败 表示验证没有通过 输出错误提示信息
                $this->error($model->getError());
                exit();
            } else {
                if ($model->add()) {
                    $this->success("添加成功", U('postimg/index'));
                } else {
                    $this->error("添加失败");
                }
            }
        }
    }
    /**
     * 更新文章信息
     * @param  [type] $id [文章ID]
     * @return [type]     [description]
     */
    public function update($id)
    {
        $id = intval($id);
        //默认显示添加表单
        if (!IS_POST) {
            $model = M('postimg')->where("id= %d",$id)->find();
            $this->assign("category",getSortedCategory(M('category')->select()));
            $this->assign('postimg',$model);
            $this->display();
        }
        if (IS_POST) {
            $model = D("postimg");
            if (!$model->create()) {
                $this->error($model->getError());
            }else{
                if ($model->save()) {
                    $this->success("更新成功", U('postimg/index'));
                } else {
                    $this->error("更新失败");
                }
            }
        }
    }
    /**
     * 删除文章
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id)
    {
        $id = intval($id);
        $model = M('postimg');
        $result = $model->where("id= %d",$id)->delete();
        if($result){
            $this->success("删除成功", U('postimg/index'));
        }else{
            $this->error("删除失败");
        }
    }
    public function push($id) {//postimg到前台
        $id = intval($id);
        if (IS_GET) {
            $status = M('postimg') -> where("id= %d",$id) -> getField('status');
            if ($status === '0') {
                $data['status'] = 1;
            } else {
                $data['status'] = 0;
            }
            $result = M('postimg') -> where("id= %d",$id) -> save($data);
            if ($result && $data['status'] === 1) {
                $this -> success("发布成功", U('postimg/index'));
            } elseif ($result && $data['status'] === 0) {
                $this -> success("撤销成功", U('postimg/index'));
            } else {
                $this -> error("操作失败");
            }
        } else {
            pass;

        }
    }

    //ajax获取文章图片
    public function getImg($id){
      if(IS_AJAX && $id)
      {
         $json_img=M('postimg')->where('id=%d',$id)->field('img')->find();
          echo $json_img;
          return true;
      }else{
          return false;
      }

    }


}
